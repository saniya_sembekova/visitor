﻿using System;

namespace Visitor.Domain {
    public class Visit: Entity {
        public int VisitorId { get; set; }
        public Visitor Visitor { get; set; }
    }
}