﻿using System;

namespace Visitor.Domain {
    public class Entity {
        public int Id { get; set; }
        public DateTime CreationDate { get; set; }
    }
}