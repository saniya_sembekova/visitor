﻿using System;
using System.Collections.Generic;

namespace Visitor.Domain {
    public class Visitor : Entity {
        public Visitor() {
            Visits = new List<Visit>();
            CreationDate = DateTime.Now;
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Patronymic { get; set; }
        public DateTime BirthDate { get; set; }
        public Gender Gender { get; set; }
        public List<Visit> Visits { get; set; }
    }
}