using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Visitor.Domain;
using Visitor.Dtos.Visit;
using Visitor.EfDal;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using AutoMapper.QueryableExtensions;

namespace Visitor.Web.Controllers
{
    [Produces("application/json")]
    [Route("/api/visits")]
    public class VisitsController : Controller
    {
        private readonly IGenericRepository<Visit> _visitRepository;

        public VisitsController(IGenericRepository<Visit> visitRepository) {
            _visitRepository = visitRepository;
        }

        [HttpGet("getvisits/{query}")]
        [HttpGet("getvisits/")]
        public IActionResult GetVisits(string query)
        {
            if (string.IsNullOrWhiteSpace(query) || string.IsNullOrEmpty(query))
            {
                return new OkObjectResult(_visitRepository.GetAll(v => v.Visitor).ProjectTo<VisitListItemDto>());
            }
            var res = _visitRepository.GetAll(v => v.Visitor)
                .Where(v => v.Visitor.FirstName.Contains(query)
               || v.Visitor.LastName.Contains(query)
               || v.Visitor.Patronymic.Contains(query))
                .ProjectTo<VisitListItemDto>();
            return new OkObjectResult(res);
        }

        [HttpGet("{id}")]
        public IActionResult GetVisit(int id)
        {
            var res = _visitRepository.GetById(id, v => v.Visitor);
            return new OkObjectResult(Mapper.Map<VisitListItemDto>(res));
        }

        [HttpPost]
        public IActionResult SaveVisit([FromBody]VisitCreateDto dto)
        {
            if ( ModelState.IsValid ) {
                var entity = Mapper.Map<Visit>(dto);
                _visitRepository.AddOrUpdate(entity);
                _visitRepository.Save();
                return new CreatedAtRouteResult("GetVisit", entity);
            }
            return NoContent();
        }
    }
}