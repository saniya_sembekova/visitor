﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Visitor.Domain;
using Visitor.Dtos.Visit;
using Visitor.Dtos.Visitor;
using Visitor.EfDal;
using Visitor = Visitor.Domain.Visitor;

namespace Visitor.Web.Controllers
{
    [Route("/api/visitors")]
    public class VisitorsController : Controller
    {
        private readonly IGenericRepository<Visit> _visitRepository;
        private readonly IGenericRepository<Domain.Visitor> _visitorRepository;

        public VisitorsController(IGenericRepository<Visit> visitRepository, IGenericRepository<Domain.Visitor> visitorRepository)
        {
            _visitRepository = visitRepository;
            _visitorRepository = visitorRepository;
        }

        [HttpGet]
        public IActionResult GetVisitors() {
            return new OkObjectResult(_visitorRepository.GetAll(  ).ProjectTo<VisitorListItemDto>(  ));
        }

        [HttpGet("searchvisitors/{query}")]
        [HttpGet("searchvisitors/")]
        public IActionResult SearchVisitors(string query)
        {
            if ( query == null ) {
                return new OkObjectResult( _visitorRepository
                                              .GetAll( v => v.Visits )
                                              .ProjectTo<VisitorListItemDto>() );
            }
            query = query.Trim();
            var res = _visitorRepository
                        .GetAll(v => v.Visits)
                        .Where(v => v.FirstName.Contains(query) 
                                    || v.LastName.Contains(query)
                                    || v.Patronymic.Contains(query));
            return new OkObjectResult(res.ProjectTo<VisitorListItemDto>());
        }

        [HttpGet("{id}")]
        public IActionResult GetVisitor(int id) {
            var res = _visitorRepository.GetById(id, v => v.Visits );
            return new OkObjectResult(Mapper.Map<VisitorListItemDto>( res ));
        }

        [HttpPost]
        public IActionResult SaveVisitor([FromBody]VisitorCreateDto dto)
        {
            if ( ModelState.IsValid ) {
                var entity = Mapper.Map<Domain.Visitor>(dto);
                _visitorRepository.AddOrUpdate(entity);
                _visitorRepository.Save();
                return new CreatedAtRouteResult("GetVisitor", entity);
            }
            return NoContent();
        }

        [HttpPut("{id}")]
        public void UpdateVisitor(int id, [FromBody]VisitorCreateDto dto)
        {
            var visitor = Mapper.Map<Domain.Visitor>(dto);
            _visitorRepository.AddOrUpdate(visitor);
        }
    }
}
