﻿using System;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Visitor.Domain;
using Visitor.Dtos.Visit;
using Visitor.Dtos.Visitor;
using Visitor.EfDal;

namespace Visitor.Web
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {
            services.AddSingleton<IConfiguration>(Configuration);
            services.AddDbContext<DataContext>( options => {
                options.UseSqlServer( Configuration.GetConnectionString( "DataContext" ) );
            });
            services.AddTransient( typeof(IGenericRepository<>), typeof(BaseRepository<>));
            Mapper.Initialize(config => {
                CreateMapForVisit( config );
                CreateMapForVisitor( config );
            });
            services.AddMvc();
        }

        private static void CreateMapForVisit( IMapperConfigurationExpression config ) {
            config.CreateMap<Visit, VisitListItemDto>()
                .ForMember( d => d.VisitorFullName,
                           m =>
                               m.MapFrom(
                                         v =>
                                             $"{v.Visitor.LastName} {v.Visitor.FirstName} {v.Visitor.Patronymic ?? String.Empty}" ) )
                .ReverseMap();
            config.CreateMap<Domain.Visit, VisitCreateDto>().ReverseMap();
        }

        private static void CreateMapForVisitor( IMapperConfigurationExpression config ) {
            config.CreateMap<Domain.Visitor, VisitorCreateDto>().ReverseMap();
            config.CreateMap<Domain.Visitor, VisitorListItemDto>()
                .ForMember(d => d.FullName,
                           m => m.MapFrom(v => $"{v.LastName} {v.FirstName} {v.Patronymic ?? String.Empty}"))
                .ReverseMap();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseCors(opt =>
            {
                opt.AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowAnyOrigin();
            });

            app.UseMvc();
        }
    }
}
