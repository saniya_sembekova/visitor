import Vue from 'vue'
import Router from 'vue-router'
import VueResource from 'vue-resource'

import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.css'
import 'vue-material-design-icons/styles.css'
import Visits from '@/components/Visits.vue'
import Visitors from '@/components/Visitors.vue'
import moment from 'moment'

Vue.use(Router)
Vue.use(VueResource)
Vue.use(VueMaterial)
Vue.use(VueMaterial.MdIcon)

Vue.filter('formatDateTime', function (value) {
  if (value) {
    return moment(String(value)).format('MM.DD.YYYY hh:mm')
  }
})
Vue.filter('formatDate', function (value) {
  if (value) {
    return moment(String(value)).format('MM.DD.YYYY')
  }
})

Vue.material.registerTheme({
  default: {
    primary: 'blue',
    accent: 'red'
  },
  green: {
    primary: 'green',
    accent: 'pink'
  },
  blue: {
    primary: 'blue',
    accent: 'white'
  }
})

export default new Router({
  routes: [
    { name: 'root', path: '/', component: Visitors },
    { name: 'visits', path: '/visits', component: Visits },
    { name: 'visitors', path: '/visitors', component: Visitors }
  ]
})
