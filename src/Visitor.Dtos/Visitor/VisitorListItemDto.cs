﻿using System;
using System.Collections.Generic;

namespace Visitor.Dtos.Visitor {
    public class VisitorListItemDto {
        public int Id { get; set; }
        public string FullName { get; set; }
        public DateTime BirthDate { get; set; }
        public List<Domain.Visit> Visits { get; set; }
        public int VisitCount {
            get { return Visits.Count; }
        }
    }
}