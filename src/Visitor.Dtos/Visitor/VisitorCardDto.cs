﻿using System;
using System.Collections.Generic;
using Visitor.Domain;
using Visitor.Dtos.Visit;

namespace Visitor.Dtos.Visitor {
    public class VisitorCardDto {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Patronymic { get; set; }
        public string Iin { get; set; }
        public DateTime BirthDate { get; set; }
        public Gender Gender { get; set; }
        public virtual List<VisitListItemDto> Visits { get; set; }
    }
}