﻿using System;
using System.ComponentModel.DataAnnotations;
using Visitor.Domain;

namespace Visitor.Dtos.Visitor {
    public class VisitorCreateDto {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Patronymic { get; set; }
        public string Iin { get; set; }
        public DateTime BirthDate { get; set; }
        public Gender Gender { get; set; }
    }
}