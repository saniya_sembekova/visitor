﻿using System;

namespace Visitor.Dtos.Visit
{
    public class VisitListItemDto
    {
        public int Id { get; set; }
        public DateTime CreationDate { get; set; }
        public int VisitorId { get; set; }
        public string VisitorFullName { get; set; }
    }
}
