﻿using System;
using System.ComponentModel.DataAnnotations;
using Visitor.Domain;
using Visitor.Dtos.Visitor;

namespace Visitor.Dtos.Visit {
    public class VisitCreateDto {
        public VisitCreateDto() {
            CreationDate = DateTime.Now;
        }
        public int Id { get; set; }
        public DateTime CreationDate { get; set; }
        public int VisitorId { get; set; }
    }
}