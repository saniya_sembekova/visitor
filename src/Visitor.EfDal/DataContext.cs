﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Visitor.Domain;

namespace Visitor.EfDal
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions options) : base(options)
        {

        }
        public DbSet<Domain.Visitor> Visitors { get; set; }
        public DbSet<Visit> Visits { get; set; }


    }
}
