﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Visitor.Domain;

namespace Visitor.EfDal {
    public interface IGenericRepository<TEntity> where TEntity: Entity {
        IQueryable<TEntity> GetAll(params Expression<Func<TEntity, object>>[] propertiesForInclude);
        TEntity GetById(int id, params Expression<Func<TEntity, object>>[] propertiesForInclude);
        void AddOrUpdate(TEntity entity);
        void Save();
        void Remove(int id);
    }
}