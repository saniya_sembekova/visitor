﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using Visitor.Domain;

namespace Visitor.EfDal {
    public class BaseRepository<TEntity> : IGenericRepository<TEntity> where TEntity:Entity {
        private readonly DataContext _context;
        private DbSet<TEntity> _set;
        public BaseRepository( DataContext context ) {
            _context = context;
            _set = _context.Set<TEntity>();
        }
        public IQueryable<TEntity> GetAll(params Expression<Func<TEntity, object>>[] propertiesForInclude) {
            var query = _set.AsQueryable();
            foreach (var prop in propertiesForInclude)
            {
                query = query.Include(prop);
            }
            return query;
        }

        public TEntity GetById( int id, params Expression<Func<TEntity, object>>[] propertiesForInclude) {
            var query = _set.Where(v=>v.Id == id );
            foreach (var prop in propertiesForInclude)
            {
                query = query.Include(prop);
            }
            return query.FirstOrDefault();
        }

        public void AddOrUpdate( TEntity entity ) {
            var existEntity = _set.Find( entity.Id );
            if ( existEntity == null ) {
                _set.Add( entity );
            } else {
                _set.Update( entity );
            }
        }

        public void Save() {
            _context.SaveChanges();
        }

        public void Remove( int id ) {
            var existEntity = _set.Find( id );
            if ( existEntity != null ) {
                _context.Entry( existEntity ).State = EntityState.Deleted;
            }
        }
    }
}