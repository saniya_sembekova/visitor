create database [Visitor]
GO
use [Visitor]
CREATE TABLE [Visitors] (
Id int identity(1,1) not null,
FirstName nvarchar(100) not null,
LastName nvarchar(100) not null,
Patronymic nvarchar(100) not null,
CreationDate datetime2 not null,
BirthDate dateTime2 NULL, 
Gender int NULL,
Constraint PK_Persons Primary key clustered (Id ASC)
)

CREATE TABLE Visits (
Id int identity(1,1) not null,
VisitorId int not null,
CreationDate datetime2 not null,
Constraint PK_Visits Primary key clustered (Id ASC),
Constraint FK_Visits_Persons Foreign key (VisitorId) references Persons (Id) on delete cascade
)
